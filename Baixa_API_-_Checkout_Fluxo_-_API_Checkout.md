### 1. Diagrama de solicitação de pagamento para clientes externos
![](https://gitlab.com/rec7241808/bb/-/raw/34fae9a19fcf4af994d67c0a36a8fb9e3a2d3955/Aspose.Words.9cf28df7-60d6-4cbc-a80d-f64840cb4abf.001.png)

### 2. Diagrama de Fluxo - API Serviço de Arrecadação BB e CCK - Proposta RFB
![](https://gitlab.com/rec7241808/bb/-/raw/main/Aspose.Words.9cf28df7-60d6-4cbc-a80d-f64840cb4abf.002.jpeg?ref_type=heads)

## 3. FLUXO DA SOLICITAÇÃO DE PAGAMENTO (APIs) 

### 3.1 GERAÇÃO - SOLICITAÇÃO DE PAGAMENTO

#### Passo 1

- Após cliente selecionar tributos, RFB chama microserviço BB (endpoint)  para gerar link de pagamento. 

Endpoint:[ https://api.externo.bb.com.br/checkout-arrecadacao/v1/requisicao- link-pagamento ](https://api.externo.bb.com.br/checkout-arrecadacao/v1/requisicao-link-pagamento)


Payload:

|<h4>Contrato de Requisição:</h4> { <br/> ` `"codigoConvenio",<br/>` `"codigoTipoPessoaDevedor",<br/>` `"codigoIdentificadorDevedor",<br/>`   `"textoUnicoDescricaoPagamento",<br/>` `"dataPagamento",<br/>` `"listaPagamento": [<br/>`  `{<br/>`   `"codigoBarraCaptura",<br/>`   `"textoDescricaoPagamento",<br/>`   `"valorPagamento"<br/>`  `}<br/>` `]<br/>}|<h4>Contrato de Resposta:</h4>{<br/>` `"codigoChaveUnico",<br/>` `"textoUniversalResourceLocatorSolicitacao"<br/>}|
|- |-|

#### Passo 2 

- Microserviço chama operação IIB ”Op6675785.v1 - Validar Guia de Arrecadação para Link de Pagamentos (RECS0904)”, para criação do link de pagamento. 

Payload:

|<h4>Contrato de Requisição:</h4> { <br/> ` `"codigoConvenio",<br/>` `"codigoTipoPessoaDevedor",<br/>` `"codigoIdentificadorDevedor",<br/>`   `"textoUnicoDescricaoPagamento",<br/>` `"dataPagamento",<br/>` `"quantidadeOcorrenciaListaPagamento",<br/>` `"listaPagamento": [<br/>`  `{<br/>`   `"codigoBarraCaptura",<br/>`   `"textoDescricaoPagamento",<br/>`   `"valorPagamento"<br/>`  `}<br/>` `]<br/>}|<h4>Contrato de Resposta:</h4>{<br/>` `"codigoChaveUnico",<br/>` `"textoUniversalResourceLocatorSolicitacao"<br/>}|
|- |-|

#### Passo 3

- A partir da “Op6675785.v1-Validar Guia de Arrecadação para Link de Pagamentos (RECS0904)” chama a operação IIB “Op5622974.v3 - Criar Solicitação de Pagamento (CCK)”. 

Payload: 

|<h4>Contrato de Requisição:</h4>{ <br/>` `"numeroConvenio": 0,<br/> ` `"timestampLimiteSolicitacaoPagamento": "",<br/> ` `"indicadorPagamentoUnico": "",<br/> ` `"siglaSistemaResponsavelCadastramento": "",<br/> ` `"codigoCanalSolicitacaoPagamento": 0,<br/> ` `"codigoClienteSubConvenio": 0,<br/> ` `"valorSolicitacaoPagamento": 0,<br/> ` `"dataVencimentoSolicitacaoPagamento": "",<br/> ` `"codigoConciliacaoSolicitacaoPagamento": "",<br/> ` `"percentualMultaPagamento": 0,<br/> ` `"valorNominalMultaPagamento": 0,<br/> ` `"percentualJuroPagamento": 0,<br/> ` `"valorAbatimentoPagamento": 0,<br/> ` `"nomeSolicitacaoPagamentoPessoaFisica": "",<br/> ` `"codigoTipoPessoaDevedor": 0,<br/> ` `"numeroIdentificadorDevedorSecretariaDaRecei taFederal": 0,<br/> ` `"numeroCepDevedor": 0,<br/> ` `"textoEnderecoDevedor": "",<br/> ` `"nomeBairroDevedor": "",<br/> ` `"nomeCidadeDevedor": "",<br/> ` `"siglaUnidadeDaFederacaoDevedor": "",<br/> ` `"textoEmailDevedor": "",<br/> ` `"numeroDiscagemDiretaADistanciaTelefoneDev edor": 0,<br/> ` `"numeroTelefoneDevedor": 0,<br/> ` `"numeroAgencia": 0,<br/> ` `"numeroContaCorrente": 0,<br/> ` `"codigoClienteAcesso": 0,<br/> ` `"codigoClientePessoaJuridica": 0,<br/> ` `"codigoClientePagador": 0,<br/> ` `"codigoTipoContaPagador": 0,<br/> ` `"codigoUnidadeOrganizacionalContaPagador": 0,<br/> ` `"numeroContaCorrentePagador": 0,<br/> ` `"numeroVariacaoPoupancaPagador": 0,<br/> ` `"numeroIdentificadorRepresentanteExterno": 0,<br/>` `"quantidadeDescontoLista": 0,<br/> ` `"quantidadePagamentoLista": 0,<br/> ` `"listaDesconto": [ <br/>`   `{ <br/>`     `"dataDescontoPagamento": "",<br/> `     `"valorNominalDescontoPagamento": 0,<br/>       `     `"percentualDescontoPagamento": 0<br/> `    `} <br/>`  `], <br/>` `"listaPagamento": [ <br/>`   `{<br/>`     `"codigoTipoPagamento": "",<br/> `     `"quantidadeParcelaPagamento": 0 <br/>`   `} <br/>`  `]<br/> } <br/>|<h4>Contrato de Resposta: </h4>{ <br/>` `"codigoChaveUnico",<br/> ` `"textoUniversalResourceLocatorSolicitacao"<br/> } |
| - | - |

#### Passo 4 

- A partir da “Op5622974.v3-Criar Solicitação de Pagamento” é feita uma requisição para a “Op6915618.v1-Gerar Solicitação de Pagamento Externa” para criação do link para o pagamento. 

Payload: 

|<h4>Contrato de Requisição:</h4>{<br/>` `"codigoCompanhiaComercial": "",<br/>` `"codigoLoteSolicitacaoPagamento": "",<br/>` `"quantidadeTamanhoTextoUniversalResourceLocat orEncaminhamentoCliente": 0,<br/>` `"quantidadeOcorrenciaListaSolicitacaoPagamento": 0,<br/>` `"listaUniversalResourceLocatorEncaminhamentoCli ente": "",<br/>` `"listaSolicitacaoPagamento": [<br/>`   `{<br/>`    `"codigoTipoSolicitacao": 0,<br/>`    `"textoChaveCorrelacaoSolicitacao": "",<br/>`    `"valorPagamento": 0,<br/>`    `"textoDescricaoResumidoSolicitacao": "",<br/>`    `"textoDescricaoSolicitacao": "",<br/>`    `"codigoSolicitacaoPagamento": 0,<br/>`    `"dataExpiracaoSolicitacao": "",<br/>`    `"codigoTipoPessoaPagador": 0,<br/>`    `"numeroContribuintePagador": 0,<br/>`    `"codigoTipoPessoaRecebedor": 0,<br/>`    `"nomeContribuinteRecebedor": "",<br/>`    `"numeroContribuinteRecebedor": 0,<br/>`    `"textoCategoria": ""<br/>`  `}<br/>` `]<br/>}|<h4>Contrato de Resposta: </h4>{<br/>` `"quantidadeTamanhoTextoUniversalResourceLoc atorSolicitacaoPagamento": 0,<br/>` `"listaTextoUniversalResourceLocatorSolicitacaoPagamento": ""}|
| - | - |

#### Passo 5 

- A partir da “Op6915618.v1-Gerar Solicitação de Pagamento Externa” é feita uma requisição para o serviço Rest da VP, para obter token de acesso. 

**Endpoint VP**: https://api-receita.cck.digital/api/public/v1/auth 

**Swagger VP:** http://alb-vp-checkout-backend-dev-565550141.us-east- 1.elb.amazonaws.com/doc/#/default/post\_public\_v1\_auth 

**Token de Acesso:**  

Payload: 

|<h4>Contrato de Requisição:</h4>@JsonbProperty("accessKey")<br/>private String accessKey; <br/> <br/>@JsonbProperty("document") private String document;<br/>|<h4>Contrato de Resposta: </h4>{<br/>  `"token": "string" <br/>}|
| - | - |

**Requisição:** 

Payload: 

|<h4>Contrato de Requisição:</h4>[<br/>`  `{<br/>`    `"type": "CHARGE",<br/>`    `"key": "string",<br/>`    `"amount": 0,<br/>`    `"shortDescription": "string",     "description": "string",<br/>`    `"externalId": "string",<br/>`    `"expirationDate": "2023-02-02",     "payerDocumentType": "CPF",<br/>`    `"payerDocument": "string",<br/>`    `"receiverDocumentType": "CPF",     "receiverDocument": "string"<br/>`  `}<br/>]|<h4>Contrato de Resposta:</h4>{<br/>`  `"link": "string" }|
| - | - |

### 3.2 LIQUIDAÇÃO - SOLICITAÇÃO DE PAGAMENTO

#### Passo 1 

- A VP aciona o serviço do CCK via API, com Oauth BB e mTLS, onde os parceiros vão utilizar a URL “checkout-pagamentos-webhook.bb.com.br”, e o BW vai chamar a URL “api.extranet.bb.com.br”. 

**Swagger serviço CCK**: http://cck-pagamentos-webhook.cck.servicos.bb.com.br/api-docs/ 

Payload: 

|<h4>Contrato de Requisição:  </h4>{<br/>`  `"numeroSolicitacaoPagamento": 15960,<br/>`  `"valorOriginalPagamento": 200,<br/>`  `"valorFinalPagamento": 205.65,<br/>`  `"dataPagamento": "2007-12- 03T10:15:30+01:00",<br/>`  `"numeroPlasticoCartaoMascarado": "4854- xxxx-xxxx-5654",<br/>`  `"nomeClientePlasticoCartao": "João D Augusto",<br/>`  `"tipoPagador": "(1) CPF, (2) CNPJ",<br/>`  `"numeroCpfCnpjPagador": "12345678900 (Se tipo pagador = 1) / 12345678000199 (Se tipo pagador = 2) ",<br/>`  `"nomeBandeiraCartao": "Visa",<br/>`  `"quantidadeParcela": 2,<br/>"codigoIdentificadorSolicitacaoAutorizacao": "66098f57-5338-4afb-1g46-79b0e613b967",<br/>`  `"codigoIdentificacaoTransacaoAdquirente": "27740874961VFT8RA3RC",<br/>`  `"codigoAutorizacaoTransacao": "985123",<br/>`  `"codigoProvaCompra": "54001",<br/>`  `"codigoReferenciaPagamento": "V0010013031108631051921903375",<br/>`  `"estabelecimentoComercial": "12345, 78910",<br/>`  `"adquirente": "CIE, STO"<br/>}|<h4>Contrato de Resposta:</h4>{ <br/>` `"comprovantePagamento": { ` `"linhasComprovantePagamento": [<br/>`  `{<br/>`   `"linhaComprovantePagamento":,<br/>`   `"Convenio:,<br/> `   `CEB"<br/>`  `}<br/>` `}<br/>} |
| - | :- |

#### Passo 2 

- O microserviço provedor do endpoint (https://checkout-pagamentos-webhook.bb.com.br), aciona a operação “Op6769645v1-Liquidar um pagamento PIX”, para liquidar a guia. 

Payload: 

|<h4>Contrato de Requisição: </h4>{ <br/>` `"numeroPagamentoSolicitacao": 0 <br/>} |<h4>Contrato de Resposta: </h4>{ <br/>` `"quantidadePagamento": 0, <br/>` `"listaPagamento": [ <br/>`  `{ <br/>   `   `"textoDescansoPagamento": "", <br/>`   `"valorPagamento": 0, <br/>`   `"dataPagamento": "", <br/>`   `"textoAutenticacao": "", <br/>`   `"quantidadeLinhaComprovante": 0`   `"textoLinhaComprovanteRetorno": [<br/>`    `{ <br/>`     `"textoLinhaComprovante": "" <br/>`    `} <br/>`    `] <br/>`   `} <br/>`  `] <br/>} 
| - | - |

#### Passo 3 

- A partir da “Op6769645v1 - Liquidar um pagamento PIX (CCK)” é chamada a “Op6776099.v1 - Liquidar Guias de Convênio por Checkout (RECS0905)” para realizar a baixa da solicitação de pagamento (codigoChaveUnico). 

Payload: 

|<h4>Contrato de Requisição:  </h4>{ <br/>` `"codigoChaveUnico" } |<h4>Contrato de Resposta: </h4>{ <br/>` `"quantidadePagamento", <br/>` `"listaPagamento": [<br/>`  `{ <br/>`    `"textoDescansoPagamento":"",<br/>`    `"valorPagamento", <br/>`    `"dataPagamento", <br/>        `    `"textoAutenticacao", <br/>`    `"quantidadeLinhaComprovante", <br/>`    `"textoLinhaComprovanteRetorno":[<br/>`     `{<br/>`      `"textoLinhaComprovante":"" <br/>`     `} <br/>`    `]<br/>`  `} <br/>` `]<br/>};|
| - | - |

